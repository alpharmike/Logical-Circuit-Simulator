from collections import Callable
from typing import Tuple, List

from flipflop.d import D_FlipFlop
from gate.not_gate import Not
from gate.one_gate import One
from gate.or_gate import Or
from gate.xor_gate import Xor
from gate.zero_gate import Zero


class Comparator:  # n-ary comparator
    def __init__(self, inputs: Tuple[List[D_FlipFlop], List[D_FlipFlop]], name="Comparator"):
        self.name = name
        self.inputs = inputs

    def logic(self):
        a, b = self.inputs[0], self.inputs[1]
        print("len a" + str(len(a)))
        compared_bits = []
        for i in range(len(a)):
            xor_input0 = One() if a[i].logic() else Zero()
            xor_input1 = One() if b[i].logic() else Zero()
            xor = Xor((xor_input0, xor_input1), name=f"{self.name}_Xor{i}")
            xor.logic()
            compared_bits.append(xor)

        output = compared_bits[0]
        if len(compared_bits) == 1:
            output = Not(output, name=f"{self.name}_Not")
            output.logic()
            return output

        for bit in compared_bits[1:]:
            output = Or((bit, output), name=f"{self.name}_Or")
            output.logic()

        output = Not(output, name=f"{self.name}_Not")
        output.logic()
        return output
