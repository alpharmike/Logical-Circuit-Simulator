from gate.and_gate import And
from gate.not_gate import Not
from gate.one_gate import One
from gate.or_gate import Or
from gate.xor_gate import Xor
from gate.zero_gate import Zero
from subtractor.half_subtractor import HalfSubtractor
from subtractor.subtractor import Subtractor


class FullSubtractor(Subtractor):
    def __init__(self, inputs, b_in, name="Full_Subtractor"):
        super().__init__(inputs, name)
        self.bin = b_in

        if inputs:
            self.build()

    def build(self):
        a, b = self.inputs[0], self.inputs[1]
        b_in = self.bin

        xor0 = Xor((a, b), f"{self.name}_xor0")

        not0 = Not(a, f"{self.name}_not0")

        and0 = And((not0, b), f"{self.name}_and0")

        xor1 = Xor((xor0, b_in), f"{self.name}_xor1")

        not1 = Not(xor0, f"{self.name}_not1")

        and1 = And((not0, b_in), f"{self.name}_and1")

        or0 = Or((and0, and1), f"{self.name}_or0")

        # hs0 = HalfSubtractor((a, b), f"{self.name}_hs0")
        # d0, bout0 = hs0.logic()
        #
        # d0 = One() if d0 else Zero()
        # bout0 = One() if bout0 else Zero()
        #
        # hs1 = HalfSubtractor((d0, b_in), f"{self.name}_hs1")
        #
        # d1, bout1 = hs1.logic()
        # d1 = One() if d1 else Zero()
        # bout1 = One() if bout1 else Zero()
        # or0 = Or((bout0, bout1), f"{self.name}_or0")

        self.difference = xor1
        self.bout = or0

    def set_bin(self, b_in):
        self.bin = b_in
        self.build()

    def b(self):
        return self.bout.output

    def __repr__(self):
        return f"{self.name}: {self.d()}, {self.b()}"

    def logic(self, depend=[]):
        if self in depend:
            return self.d(), self.b()

        self.difference.logic(depend + [self])
        self.bout.logic(depend + [self])

        return self.d(), self.b()