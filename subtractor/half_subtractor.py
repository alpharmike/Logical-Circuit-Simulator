from gate.and_gate import And
from gate.not_gate import Not
from gate.xor_gate import Xor
from subtractor.subtractor import Subtractor


class HalfSubtractor(Subtractor):
    def __init__(self, inputs, name="Half_Subtractor"):
        super().__init__(inputs, name)

        if inputs:
            self.build()

    def build(self):
        a, b = self.inputs[0], self.inputs[1]

        xor0 = Xor((a, b), f"{self.name}_xor0")

        not0 = Not(a, f"{self.name}_not0")

        and0 = And((not0, b), f"{self.name}_and0")

        self.difference = xor0
        self.bout = and0

    def b(self):
        return self.bout.output

    def __repr__(self):
        return f"{self.name}: {self.d()}, {self.b()}"

    def logic(self, depend=[]):
        if self in depend:
            return self.d(), self.b()

        self.difference.logic(depend + [self])
        self.bout.logic(depend + [self])

        return self.d(), self.b()
