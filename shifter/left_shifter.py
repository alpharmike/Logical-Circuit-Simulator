from gate.zero_gate import Zero
from multiplexer.mux2x1 import Mux2x1


class LeftShifter:
    def __init__(self, inputs, shift, name="Shifter"):
        self.input = inputs
        self.shift = shift
        self.name = name
        self.output = None

        self.build()

    def build(self):
        muxs = [Mux2x1((None, None), [self.shift], name=f"mux_{i}") for i in range(len(self.input))]

        for i in range(len(self.input)):
            muxs[len(self.input) - i - 1].set_inputs((self.input[len(self.input) - i - 1], Zero() if i == 0 else self.input[len(self.input) - i]))

        self.output = muxs

    def logic(self, depend=[]):
        for o in self.output:
            o.logic(depend + [self])

        return self.output
