from shifter.right_shifter import RightShifter


class NBitRightShifter:
    def __init__(self, inputs, shift, n, name="NBitRightShifter"):
        self.input = inputs
        self.shift = shift
        self.n = n
        self.name = name
        self.output = None

        self.build()

    def build(self):
        s_input = self.input
        for i in range(self.n):
            s_input = RightShifter(s_input, self.shift).logic()

        self.output = s_input

    def logic(self, depend=[]):
        for o in self.output:
            o.logic(depend + [self])

        return self.output
