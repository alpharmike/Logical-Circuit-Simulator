from flipflop.d import D_FlipFlop
from gate.one_gate import One
from gate.zero_gate import Zero
from signals.signal import Signal


class Register:  # all registers are 32-bit in mips
    def __init__(self, clock):
        self.clock = clock
        self.bits = self.build()

    def build(self):
        stored_bits = []

        for i in range(32):
            d_flip_flop = D_FlipFlop(self.clock, Zero())
            d_flip_flop.set_input(Zero())
            d_flip_flop.reset()
            stored_bits.append(d_flip_flop)
        return stored_bits
