from adder.full_adder import FullAdder
from flipflop.d import D_FlipFlop
from gate.zero_gate import Zero
from signals.signal import Signal


# class NBitAdder:
#     def __init__(self, input_size, a, b, clock):
#         self.input_size = input_size
#         self.a = a
#         self.b = b
#         self.clock = clock
#
#     def logic(self):
#         # clock = Signal()
#
#         adder = [FullAdder(None, None, f"adder{i}") for i in range(self.input_size)]
#
#         res = [D_FlipFlop(self.clock, None, f"r{i}") for i in range(self.input_size)]
#
#         for i in range(self.input_size):
#             adder[self.input_size - i - 1].set_input((self.a[self.input_size - i - 1], self.b[self.input_size - i - 1]))
#             adder[self.input_size - i - 1].set_cin(Zero() if i == 0 else adder[self.input_size - i].cout)
#
#             res[self.input_size - i - 1].set_input(adder[self.input_size - i - 1].sum)
#             res[self.input_size - i - 1].reset()
#
#         for _ in range(self.input_size):
#             self.clock.pulse()
#             for i in range(self.input_size):
#                 res[i].logic()
#
#         # print("".join([str(r.q()) for r in self.a]))
#         # print("".join([str(r.q()) for r in self.b]))
#         # print("".join([str(r.q()) for r in res]))
#
#         return res

class NBitAdder:
    def __init__(self, a, b, n, clock=None, name="NBitAdder"):
        self.n = n
        self.a = a
        self.b = b
        self.clock = Signal() if clock is None else clock
        self.name = name

        self.outputs = None

        self.build()

    def build(self):

        # d1 = [D_FlipFlop(self.clock, None, f"a{i}") for i in range(self.n)]
        # d2 = [D_FlipFlop(self.clock, None, f"b{i}") for i in range(self.n)]

        adder = [FullAdder(None, None, f"adder{i}") for i in range(self.n)]

        res = [D_FlipFlop(self.clock, None, f"r{i}") for i in range(self.n)]

        for i in range(self.n):
            adder[self.n - i - 1].set_input((self.a[self.n - i - 1], self.b[self.n - i - 1]))
            adder[self.n - i - 1].set_cin(Zero() if i == 0 else adder[self.n - i].cout)

            res[self.n - i - 1].set_input(adder[self.n - i - 1].sum)
            res[self.n - i - 1].reset()

        # for _ in range(self.n):
        #     self.clock.pulse()
        #     for i in range(self.n):
        #         res[i].logic()

        self.outputs = res

    def logic(self, depend=[]):
        # for _ in range(self.n):
            # self.clock.pulse()
        for i in range(self.n):
            self.outputs[i].logic(depend + [self])

        return self.outputs
