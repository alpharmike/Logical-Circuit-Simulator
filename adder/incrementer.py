from adder.n_bit_adder import NBitAdder
from gate.one_gate import One
from gate.zero_gate import Zero


class Incrementer:
    def __init__(self, inputs):
        self.input = inputs
        self.output = None

        self.build()

    def build(self):
        b = [Zero() for i in range(len(self.input) - 1)]
        b.append(One())

        adder = NBitAdder(self.input, b, len(self.input))

        self.output = adder

    def logic(self, depend=[]):
        o = self.output.logic(depend + [self])
        self.output = o
        return self.output
